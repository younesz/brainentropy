function ax   =   be_show_tf(TF, TA, FA, wtp, varargin)
% This function displays continuous wavelet TF planes and ridges planes 
% provided in TF. 
%
% INPUTS:
% -------
%   -   TF          :   time-frequency plane, either wavelet transform and 
%                       ridge plane [double, Nfreq x Ntime]
%   -   TA          :   time vector [double, 1xNtime]
%   -   FA          :   frequency vector [double, 1xNfreq]
%   -   wtp         :   describe the TF plane to be displayed 
%                       [char, {'cwt', 'rdg'}]
%   -   AX          :   axes where the plot should be put [double, 1]
%   -   FLIMS       :   frequency limits in Hz for the plot [double, 1x2]
%   -   TLIMS       :   time limits in sec for the plot [double, 1x2]
%   -   yOff        :   remove or keep the y axis [boolean, 1]
%   -   nRows       :   number of rows for figure subdivision [double, 1]
%   -   TIT         :   title [char, 1xN]
%   -   rdgs        :   ridges to highlight [cell, 1xN]
%   -   normalize   :   normalize TF power, either for all planes or by
%                       rows [char, {'rows', 'all'}]
%
% OUTPUTS:
% --------
%   -   AX          :   axis handle [double, 1]
%
% -------------------------------------------------------------------------
%
% YZ, 2015 (CHUM+POLY)
%
% -------------------------------------------------------------------------

fprintf('\n\n***\tBE_SHOW_TF.m\t***\n')

%% ====---- PROCESS INPUTS
% TF plane
if ~iscell(TF)
    TF      =   {TF};
end
% Container : axes
if (numel(varargin)>0)&(ishandle(varargin{1}))
    ax = varargin{1};
else
    figure; 
    ax = axes;
end

FLIMS       =   FA([1 end]);
if numel(varargin)>1
    FLIMS   =   varargin{2};
end

XLIMS       =   TA([1 end]);
if numel(varargin)>2 && ~isempty(varargin{3})
    XLIMS   =   varargin{3};
end

yoff        =   0;
if numel(varargin)>3 && ~isempty(varargin{4})
    yoff    =   varargin{4};
end

TIT         =   false;
if numel(varargin)>4 && ischar(varargin{5})
    TIT     =   varargin{5};
end

if numel(varargin)>5 && iscell(varargin{6})
    rdgs    =   varargin{6};
end

% get CLIM
iD      =   be_closest(FLIMS, FA);
tiD 	=   be_closest(XLIMS, TA);

% TF/ridge plane limits
Ytic    =   2.^(fix( log2(FA(1)) ):.5:fix( log2(FA(end)) ));
temp    =   num2str(Ytic','%4.0f');
Yticl   =   mat2cell( temp, ones(1, numel(Ytic)), size(temp,2) );
minF    =   1;

% Draw plane
    switch wtp
        case 'cwt'
            imagesc(TA, log2(FA), TF{ii},'parent', ax(ii) );
            if isempty(TIT)
                title(' Average wavelet transform ','FontSize', 16, 'FontWeight', 'bold');
            elseif ii<=nCol
                title(TIT{ii},'FontSize', 18, 'interpreter', 'latex');
            end
            
        case 'rdg'
            imagesc(TA, log2(FA), TF{ii},'parent', ax(ii) );
            if isempty(TIT)
                title(' Multivariate ridge plan ','FontSize', 16, 'FontWeight', 'bold');
            else
                title(TIT{ii},'FontSize', 18, 'interpreter', 'latex');
            end
                        
        case 'mask'
            mask    =   zeros( size(TF{ii}) );
            mask([rdgs{:}]) = 1;
            imagesc(TA, log2(FA), mask,'parent', ax(ii) );
            hold on
            nfrqs   =   numel( FA );
            for jj  =   1 : numel(rdgs)
                rl  =   rdgs{jj};
                tl  =   TA( fix( mean(rl/nfrqs) ) );
                fl  =   FA( fix( mean( mod(rl, nfrqs) ) ) +3 );
                text(tl, log2(fl), num2str(jj), 'FontSize', 12, 'Color', [1 1 1], 'horizontalAlignment', 'center', 'interpreter', 'latex');
            end
            if ~isempty(TIT)
                title('Thresholded ridge plane ','interpreter', 'latex', 'FontSize', 16, 'FontWeight', 'bold');
            end
            colormap('gray')
    end

    if ii> ((nRow-1)*nCol)
        xlabel('time (s)', 'FontSize',16, 'FontWeight', 'demi'); 
    end
    
end

% Normalize display
switch normalize
    
    case 'all'
        ALLpl   =   cat(3, TF{:});
        ALLpl   =   ALLpl(iD(1):iD(end),tiD(1):tiD(end),:);
        CLIM    =   [min(ALLpl(:)) max(ALLpl(:))];
        
        set(ax(:), 'XLim', TA([1 end]), 'YGrid','on', 'YLim',log2([minF FA(end)]),...
            'YTick',log2(Ytic(:)), 'YTickLabel',Yticl, 'YDir', 'normal', ...
            'FontName','Helvetica', 'FontSize',16, ...
            'FontWeight', 'bold', 'clim', CLIM, ...
            'ylim', log2(FLIMS), 'xlim', XLIMS )
        
    case 'rows'
        for ii  =   1 : nRow
            idPlane =   (1:nCol)+(ii-1)*nCol; 
            idPlane =   idPlane(idPlane<=numel(TF));
            ALLpl   =   cat(3, TF{idPlane});
            ALLpl   =   ALLpl(iD(1):iD(end),tiD(1):tiD(end),:);
            CLIM    =   [min(ALLpl(:)) max(ALLpl(:))];
        
            set(ax(idPlane), 'XLim', TA([1 end]), 'YGrid','on', 'YLim',log2([minF FA(end)]),...
            'YTick',log2(Ytic(:)), 'YTickLabel',Yticl, 'YDir', 'normal', ...
            'FontName','Helvetica', 'FontSize',16, ...
            'FontWeight', 'bold', 'clim', CLIM, ...
            'ylim', log2(FLIMS), 'xlim', XLIMS )
        end
        
end
     
% Display y-label
for ii  =   1 : nbTF
    
    ax(ii)  =   subplot(nRow, nCol,ii);
    if ii==1 || mod(ii, nCol)==1
        ylabel('frequency (Hz)','FontSize',16, 'FontWeight', 'demi');
    else
        set( ax(ii), 'ytick', [], 'yticklabel', '');
    end

end


return
